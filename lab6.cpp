/*********
David Abdelsayed
CPSC 1021,003,F20
dabdels@g.clemson.edu
TAs: Elliot McMillan and Victoria Xu
**********/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;


typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));


  /*Create an array of 10 employees and fill information from standard input with prompt messages*/
	employee employess[10];
	for(int i=0;i<10;i++)
	{
		cout <<"employee: "<< i+1 << endl;
		cout << "Enter employee's Last Name : " ;
		cin >> employess[i].lastName;
  
		cout << "Enter employee's First Name : " ;
		cin >> employess[i].firstName;

		cout << "Enter employee's birth year : " ;
		cin >> employess[i].birthYear;

		cout << "Enter employee's hourly wage : " ;
		cin >> employess[i].hourlyWage;
		cout <<endl;
	}
  /*After the array is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
	random_shuffle(&employess[0],&employess[10],myrandom);
	/*cout <<"********************\n";
	cout << "Printing before sorting " << endl;

	for(int i=0;i<10;i++)
	{
		cout <<"employee: "<< i+1 << endl;
		cout << "employee's Last Name : " ;
		cout << employess[i].lastName << endl;
  
		cout << "employee's First Name : " ;
		cout << employess[i].firstName << endl;

		cout << "employee's birth year : " ;
		cout << employess[i].birthYear << endl;

		cout << "employee's hourly wage : " ;
		cout << employess[i].hourlyWage << endl << endl;
	}*/
	


   /*Build a smaller array of 5 employees from the first five cards of the array created
    *above*/
	employee new_employess[5];
	for(int i=0;i<5;i++)
	{
		new_employess[i].lastName = employess[i].lastName;
		new_employess[i].firstName = employess[i].firstName;
		new_employess[i].birthYear = employess[i].birthYear;
		new_employess[i].hourlyWage = employess[i].hourlyWage;
	}


    /*Sort the new array.  Links to how to call this function is in the specs
     *provided*/
	sort(&new_employess[0],&new_employess[5],name_order);

    /*Now print the array below */
	for(int i=0;i<5;i++)
	{

		cout << setw(10) << new_employess[i].lastName << ", ";
		cout << setw(10) << new_employess[i].firstName << endl;
		cout << setw(22) << new_employess[i].birthYear << endl;
		cout << setw(22) << fixed << setprecision(2) << showpoint;
		cout << new_employess[i].hourlyWage  << endl << endl;

	}


  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool name_order(const employee& lhs, const employee& rhs) {
	return (lhs.lastName < rhs.lastName);
}

